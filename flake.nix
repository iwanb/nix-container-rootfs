{
  description = "Run containers directly from the store";

  outputs = { self, nixpkgs }: {

    hello-container = let pkgs = nixpkgs.legacyPackages.x86_64-linux; in pkgs.dockerTools.buildImage {
        name = "hello";
        tag = "latest";
        copyToRoot = pkgs.hello;
        config = { cmd = [ "/bin/hello" ]; };
    };

    nixos-container = let pkgs = nixpkgs.legacyPackages.x86_64-linux;
    system-container-root = (system:
      let
        pkgs = system.pkgs;
      in (system.pkgs.runCommand "nixos-container-root" {
        nixos = system.config.system.build.toplevel;
      } ''
        export PATH=${pkgs.coreutils}/bin
        cp -r $nixos/ $out
        chmod u+w $out
        rm -f $out/etc
        mkdir $out/etc
        mkdir $out/sbin
        cp $out/init $out/sbin/init
        cp -r $nixos/etc/* $out/etc/
      '')
    ); in (system-container-root (nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules = [
        ({ pkgs, modulesPath, ... }: {
          imports = [
            "${toString modulesPath}/virtualisation/docker-image.nix"
          ];
          boot.isContainer = true;
          services.journald.console = "/dev/console";
          services.getty.autologinUser = "root";
          services.nginx.enable = true;
        })
      ];
    }));
  };
}
